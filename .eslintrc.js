module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
    sourceType: "module", // Allows for the use of imports
  },
  settings: {
    react: {
      version: "detect", // Tells eslint-plugin-react to automatically detect the version of React to use
    },
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:@typescript-eslint/recommended",
    "prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    "plugin:prettier/recommended", // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors.This is always the last configuration in the extends
  ],
  // plugins: ["@typescript-eslint"],
  rules: {
    "react/prop-types": 0,
    "@typescript-eslint/explicit-function-return-type": 0,
  },
  overrides: [
    {
      files: ["*.{test}.{js,ts,tsx}", "**/__tests__/**/*.{js,ts,tsx}"],
      env: {
        jest: true,
      },
    },
  ],
};
